package com.hitomi.smlibrary;

import android.support.v4.app.Fragment;

/**
 * Created by anjukansal on 28/10/17.
 */

public interface ISelectFragment {

    void onSelectFrag(Fragment frag);
}
