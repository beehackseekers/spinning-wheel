package com.hitomi.spinmenu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.hitomi.smlibrary.ISelectFragment;
import com.hitomi.smlibrary.OnSpinMenuStateChangeListener;
import com.hitomi.smlibrary.SpinMenu;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ISelectFragment {



    private SpinMenu spinMenu;
    int wait_minutes = 1;
    String claimGiftUrl = "http://192.168.1.93:8666/spinningwheel?uuid=hheiefksh-jdjw";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String lastPlayed = sharedPreferences.getString("GbSpinningWheelPlayed","");

        if(lastPlayed != null && !lastPlayed.isEmpty()){
            DateTime lastPlayedDate = new DateTime(lastPlayed);
            if(DateTime.now().isBefore(lastPlayedDate.plusMinutes(wait_minutes))){
                Intent i = new Intent(this, AlreadyPlayedActivity.class);

                //Fire that second activity
                startActivity(i);
            }
        }

        spinMenu = (SpinMenu) findViewById(R.id.spin_menu);

        List<String> hintStrList = new ArrayList<>();
        hintStrList.add("Select your Gift");
        hintStrList.add("Select your Gift");
        hintStrList.add("Select your Gift");
        hintStrList.add("Select your Gift");
        hintStrList.add("Select your Gift");
        hintStrList.add("Select your Gift");
        hintStrList.add("Select your Gift");
        hintStrList.add("Select your Gift");

        spinMenu.setHintTextStrList(hintStrList);
        spinMenu.setHintTextColor(Color.parseColor("#DDA0DD"));
        spinMenu.setHintTextSize(14);

        spinMenu.setEnableGesture(true);
        spinMenu.setParentActivity(this);


        final List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(Fragment1.newInstance());
        fragmentList.add(Fragment2.newInstance());
        fragmentList.add(Fragment3.newInstance());
        fragmentList.add(Fragment4.newInstance());
        fragmentList.add(Fragment5.newInstance());
        fragmentList.add(Fragment6.newInstance());
        fragmentList.add(Fragment7.newInstance());
        fragmentList.add(Fragment8.newInstance());
        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }
        };
        spinMenu.setFragmentAdapter(fragmentPagerAdapter);

        spinMenu.setOnSpinMenuStateChangeListener(new OnSpinMenuStateChangeListener() {
            @Override
            public void onMenuOpened() {
                Toast.makeText(MainActivity.this, "SpinMenu opened", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMenuClosed() {
                Toast.makeText(MainActivity.this, "SpinMenu closed", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onSelectFrag(Fragment frag) {
        new GetPrize().execute();
    }

    private class GetPrize extends AsyncTask<Void, Void, String> {

        final String TAG = "GetPrize";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MainActivity.this,"Applying gift",Toast.LENGTH_LONG).show();
        }

        @Override
        protected String doInBackground(Void... arg0) {
            String imageUrl= null;
            HttpHandler sh = new HttpHandler();
            String jsonStr=null;
            // Making a request to url and getting response
            try {
                jsonStr = sh.makeGetCall(claimGiftUrl);
            }catch(Exception e){
                Log.e(TAG,"Server Not working");
                Toast.makeText(MainActivity.this,"ServerTimeout error",Toast.LENGTH_LONG).show();

            }
            Log.e(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONObject prize=jsonObj.getJSONObject("prize");
                    String gift=prize.getString("gift");
                    imageUrl=prize.getString("imageUrl");

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                }

            } else {

                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }

            return imageUrl;
        }

        @Override
        protected void onPostExecute(String imageUrl) {
            Intent i = new Intent(MainActivity.this, GiftDisplayActivity.class);

            //Create the bundle
            Bundle bundle = new Bundle();
            bundle.putString("imageUrl", imageUrl);
            i.putExtras(bundle);

            //Fire that second activity
            startActivity(i);

        }
    }

}
