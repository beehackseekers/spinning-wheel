package com.hitomi.spinmenu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import static com.hitomi.spinmenu.R.id.imageView;

public class GiftDisplayActivity extends AppCompatActivity{

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift_display);

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        //imageView.setImageResource(R.drawable.bg_fragment_2);
        final Button button = (Button) findViewById(R.id.claimPrize);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                //Fire that second activity
                startActivity(i);
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();
        editor.putString("GbSpinningWheelPlayed",new DateTime().toString());
        editor.commit();

        //Get the bundle
        Bundle bundle = getIntent().getExtras();
        String imageUrl = bundle.getString("imageUrl");

        Picasso.with(getBaseContext()).load(imageUrl).into(imageView);
    }
    public void shareButton1(View v) {
        Button btn = (Button) v;
        String message = "I won Rewards and Free Gift at Gwynniebee, Join at https://closet.gwynniebee.com/";
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message);

        startActivity(Intent.createChooser(share, "Share to Help"));
        ((Button) v).setText("Share");

    }
}
